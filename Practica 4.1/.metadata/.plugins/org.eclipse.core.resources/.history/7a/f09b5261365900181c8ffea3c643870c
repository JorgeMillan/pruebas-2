package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestEliminarCliente {

	static Cliente unCliente;
	static Cliente dosCliente;
	static Cliente tresCliente;
	static Cliente cuatroCliente;
	static Factura unaFactura;
	static Factura dosFactura;
	static Factura tresFactura;
	static GestorContabilidad unGestor;
	
	@BeforeAll
	public static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		dosCliente = new Cliente("12354456F", "Jor", LocalDate.now());
		tresCliente = new Cliente("12343456F", "Jor", LocalDate.now());
		cuatroCliente = new Cliente("1456F", "Jor", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, unCliente);
		dosFactura = new Factura("2", LocalDate.now(), "Salchipapas", 6, 7, dosCliente);
		tresFactura = new Factura("3", LocalDate.now(), "Salchipapas", 8, 9, tresCliente);


		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaFacturas().clear();
		unGestor.getListaClientes().clear();
	}
	
	//Elimina un cliente que existe y no tiene facturas
	
	@Test
	void eliminarClienteExistenteSinFacturas() {
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(dosCliente);
		unGestor.getListaClientes().add(tresCliente);
		unGestor.getListaClientes().add(cuatroCliente);
		unGestor.getListaFacturas().add(unaFactura);
		unGestor.getListaFacturas().add(dosFactura);
		unGestor.getListaFacturas().add(tresFactura);
		unGestor.eliminarCliente("1456F");
		assertFalse(unGestor.getListaClientes().contains(cuatroCliente));
	}
	
	//Elimina un cliente que no existe, asi que no deberia de eliminar nada
	
	@Test
	void eliminarClienteInexistenteSinFacturas() {
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(dosCliente);
		unGestor.getListaClientes().add(tresCliente);
		unGestor.getListaClientes().add(cuatroCliente);
		unGestor.getListaFacturas().add(unaFactura);
		unGestor.getListaFacturas().add(dosFactura);
		unGestor.getListaFacturas().add(tresFactura);
		unGestor.eliminarCliente("1454326F");
		assertTrue(unGestor.getListaClientes().contains(unCliente));
		assertTrue(unGestor.getListaClientes().contains(dosCliente));
		assertTrue(unGestor.getListaClientes().contains(tresCliente));
		assertTrue(unGestor.getListaClientes().contains(cuatroCliente));
	}
	
	@Test
	void eliminarClienteExistenteConFacturas() {
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(dosCliente);
		unGestor.getListaClientes().add(tresCliente);
		unGestor.getListaClientes().add(cuatroCliente);
		unGestor.getListaFacturas().add(unaFactura);
		unGestor.getListaFacturas().add(dosFactura);
		unGestor.getListaFacturas().add(tresFactura);
		unGestor.eliminarCliente("1456F");
		assertFalse(unGestor.getListaClientes().contains(cuatroCliente));
	}
	
	
	

}
