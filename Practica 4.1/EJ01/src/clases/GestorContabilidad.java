package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}

	public Cliente buscarCliente(String dni) {
		for( Cliente unCliente : listaClientes) {
			if(unCliente.getDni().equalsIgnoreCase(dni)) {
				return unCliente;
			}
			else {
				return null;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo) {
		for(Factura unaFactura : listaFacturas) {
			if(unaFactura.getCodigo().equalsIgnoreCase(codigo)) {
				return unaFactura;
			}
			else {
				return null;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente cliente) {
		boolean coincide = false;
		for(Cliente unCliente : listaClientes) {
			if(cliente.getDni().equalsIgnoreCase(unCliente.getDni())) {
				coincide = true;
				break;
			}
			else {
				coincide = false;
			}
		}
		if(coincide == false) {
			listaClientes.add(cliente);
		}
	}
	
	public void crearFactura(Factura factura) {
		boolean coincide = false;
		for(Factura unaFactura : listaFacturas) {
			if(factura.getCodigo().equalsIgnoreCase(unaFactura.getCodigo())) {
				coincide = true;
				break;
			}
			else {
				coincide = false;
			}
		}
		if(coincide == false) {
			listaFacturas.add(factura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		LocalDate max = LocalDate.MAX;
		Cliente masAntiguo = null;
		for(Cliente unCliente : listaClientes) {
			if(unCliente.getFechaAlta().isBefore(max)) {
				max = unCliente.getFechaAlta();
				masAntiguo = unCliente;
			}
		}
		return masAntiguo;
		
	}
	
	public Factura facturaMasCara() {
		float maxFactura = 0F;
		Factura max = null;
		for(Factura unaFactura : listaFacturas) {
			if((unaFactura.getCantidad()*unaFactura.getPrecioUnidad()) > maxFactura) {
				maxFactura = (unaFactura.getCantidad()*unaFactura.getPrecioUnidad());
				max = unaFactura;
			}
		}
		return max;
	}
	
	public float facturacionAnual(int anno) {
		float total = 0F;
		for( Factura unaFactura : listaFacturas) {
			if (unaFactura.getFecha().getYear() == anno) {
				total = total + (unaFactura.getCantidad()*unaFactura.getPrecioUnidad());
			}
		}
		
		return total;
	}
	
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Cliente cliente = null;
		for(Cliente unCliente : listaClientes) {
			if (unCliente.getDni().equalsIgnoreCase(dni)) {
				cliente = unCliente;
			}
		}
		for(Factura unaFactura : listaFacturas) {
			if(unaFactura.getCodigo().equalsIgnoreCase(codigoFactura)) {
				unaFactura.setCliente(cliente);
			}
		}
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int contador = 0;
		for(Factura unaFactura : listaFacturas) {
			if(dni.equalsIgnoreCase(unaFactura.getCliente().getDni()))
			contador++;
		}
		return contador;
		
	}
	
	public void eliminarFactura(String codigo) {
		Factura eliminar = null;
		for(Factura unaFactura : listaFacturas) {
			if(unaFactura.getCodigo().equalsIgnoreCase(codigo)) {
				eliminar = unaFactura;
			}
		}
		listaFacturas.remove(eliminar);
	}
	
	public void eliminarCliente(String dni) {
		Cliente eliminar = null;
		for(Cliente unCliente : listaClientes) {
			if(unCliente.getDni().equalsIgnoreCase(dni)) {
				for(Factura unaFactura : listaFacturas) {
					if(unaFactura.getCliente().getDni() == unCliente.getDni()) {
						break;
					}
					else {
						eliminar = unCliente;
					}
				}
			}
		}
		listaClientes.remove(eliminar);
	}
	
	
	
	
	
	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	
	
	
	
}