package clases;

import java.time.LocalDate;

public class Factura {
	private String codigo;
	private LocalDate fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente cliente;
	
	public Factura() {
		this.codigo = "";
		this.fecha = null;
		this.nombreProducto = "";
		this.precioUnidad = 0;
		this.cantidad = 0;
		this.cliente = new Cliente();
	}
	
	public Factura(String codigo, LocalDate fecha, String nombreProducto, float precioUnidad, int cantidad, Cliente cliente) {
		this.codigo = codigo;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		this.cliente = cliente;
	}

	
	
	
	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}