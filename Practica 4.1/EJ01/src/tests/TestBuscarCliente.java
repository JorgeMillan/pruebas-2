package tests;

import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestBuscarCliente {
	
	static Cliente unCliente;
	static Factura unaFactura;
	static GestorContabilidad unGestor;
	
	
	@BeforeAll
	static void preparar() {
		unCliente = new Cliente();
		unaFactura = new Factura();
		unGestor = new GestorContabilidad();
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
	}
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
	}

	
	//Busca un cliente que existe y debe encontrarlo
	@Test
	void buscarClienteExistente() {
		unGestor.getListaClientes().add(unCliente);
		Cliente actual = unGestor.buscarCliente("123456F");
		assertSame(unCliente, actual);
	}
	
	//Busca un cliente que no existe y debe dar null

	@Test
	void buscarClienteInexistente() {
		unGestor.getListaClientes().add(unCliente);
		Cliente actual = unGestor.buscarCliente("14B");
		assertNull(actual);
	}
	
	//Busca un cliente sin que haya clientes metidos
	
	@Test
	void buscarClienteSinClientes() {
		Cliente actual = unGestor.buscarCliente("123456F");
		assertNull(actual);
	}

}
