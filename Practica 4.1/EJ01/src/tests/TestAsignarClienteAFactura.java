package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestAsignarClienteAFactura {


	static Cliente unCliente;
	static Factura unaFactura;
	static GestorContabilidad unGestor;
	
	@BeforeAll
	static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, null);
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
		unGestor.getListaFacturas().clear();
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaFacturas().add(unaFactura);
	}
	
	//Comprobar que asigna bien el cliente existente a una factura existente
	
	@Test
	void asignarClienteAFactura() {

		unGestor.asignarClienteAFactura("123456F", "1");
		assertTrue(unCliente == unaFactura.getCliente());
	}
	
	//Comprobar que no puede a�adir el cliente a una factura que no existe
	
	@Test
	void asignarClienteAFacturaInexistente() {
		unGestor.asignarClienteAFactura("123456F", "2");
		assertNull(unaFactura.getCliente());
	}
	
	//Comprobar que no puede a�adir el cliente que no existe a ala factura
	
	@Test
	void asignarClienteInexistenteAFactura() {
		unGestor.asignarClienteAFactura("1234543256F", "1");
		assertNull(unaFactura.getCliente());
	}
	
	//Comprobar que no puede a�adir un cliente inexistente a una factura inexistente
	
	@Test
	void asignarClienteInexistenteAFacturaInexistente() {
		unGestor.asignarClienteAFactura("1234543256F", "2");
		assertNull(unaFactura.getCliente());
	}

}
