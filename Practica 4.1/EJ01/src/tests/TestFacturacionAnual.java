package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestFacturacionAnual {
	
	static Cliente unCliente;
	static Factura unaFactura;
	static Factura dosFactura;
	static Factura tresFactura;

	static GestorContabilidad unGestor;
	
	@BeforeAll
	static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, null);
		unaFactura = new Factura("2", LocalDate.now().minusYears(2), "Salchipapas", 5, 6, null);
		unaFactura = new Factura("3", LocalDate.now().minusYears(2), "Salchipapas", 5, 6, null);
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
		unGestor.getListaFacturas().clear();
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaFacturas().add(unaFactura);
		unGestor.getListaFacturas().add(dosFactura);
		unGestor.getListaFacturas().add(tresFactura);
	}
	
	//Facturacion de un a�o con dos facturas
	
	@Test 
	void facturacionAnual2016() {
		float actual = unGestor.facturacionAnual(2016);
		assertEquals(60, actual);
	}
	
	//Facturacion de un a�o sin facturas
	
	@Test 
	void facturacionAnual2015() {
		float actual = unGestor.facturacionAnual(2015);
		assertEquals(0, actual);
	}
	
	//Facturacion de un a�o con 1 factura
	@Test 
	void facturacionAnual2018() {
		float actual = unGestor.facturacionAnual(2018);
		assertEquals(30, actual);
	}
	

}
