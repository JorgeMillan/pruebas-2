package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestCantidadFacturasPorCliente {


	static Cliente unCliente;
	static Cliente dosCliente;
	static Cliente tresCliente;
	static Factura unaFactura;
	static Factura dosFactura;
	static Factura tresFactura;
	static Factura cuatroFactura;
	static Factura cincoFactura;
	static Factura seisFactura;
	static GestorContabilidad unGestor;
	
	
	@BeforeAll
	static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		dosCliente = new Cliente("12345436F", "Jorge", LocalDate.now());
		tresCliente = new Cliente("123232456F", "Jormullin", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, unCliente);
		dosFactura = new Factura("2", LocalDate.now(), "Salchipapas", 5, 6, dosCliente);
		tresFactura = new Factura("3", LocalDate.now(), "Salchipapas", 5, 6, dosCliente);
		cuatroFactura = new Factura("4", LocalDate.now(), "Salchipapas", 5, 6, tresCliente);
		cincoFactura = new Factura("5", LocalDate.now(), "Salchipapas", 5, 6, tresCliente);
		seisFactura = new Factura("6", LocalDate.now(), "Salchipapas", 5, 6, tresCliente);
		unGestor = new GestorContabilidad();
		}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
		unGestor.getListaFacturas().clear();
	}
	
	/*Comprobar que hace bien su trabajo contando las facturas de un cliente por su dni
	 * unCliente = 1 factura
	 * dosCliente = 2 facturas
	 * tresCliente = 3 facturas
	 */
	@Test
	void cantidadFacturasPorClienteConTresClientes() {
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(dosCliente);
		unGestor.getListaClientes().add(tresCliente);
		unGestor.getListaFacturas().add(unaFactura);
		unGestor.getListaFacturas().add(dosFactura);
		unGestor.getListaFacturas().add(tresFactura);
		unGestor.getListaFacturas().add(cuatroFactura);
		unGestor.getListaFacturas().add(cincoFactura);
		unGestor.getListaFacturas().add(seisFactura);
		int actual = unGestor.cantidadFacturasPorCliente("123232456F");
		assertEquals(3, actual);
	}
	
	//Comprobar que sale cero si no hay ningun cliente o factura metida y con dni metido como null
	
	@Test
	void cantidadFacturasPorClienteSinClientesSinFacturas() {
		int actual = unGestor.cantidadFacturasPorCliente(null);
		assertEquals(0, actual);
	}
}
