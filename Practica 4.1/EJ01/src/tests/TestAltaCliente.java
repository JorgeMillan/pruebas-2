package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestAltaCliente {
	
	static Cliente unCliente;
	static Factura unaFactura;
	static GestorContabilidad unGestor;

	@BeforeAll
	public static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
		unGestor.altaCliente(unCliente);
	}
	
	//a�ade un cliente que no existia y debe a�adirlo
	
	@Test
	void a�adirClienteDNIInexistente() {
		boolean actual = unGestor.getListaClientes().contains(unCliente);
		assertTrue(actual);
	}
	
	//a�ade un cliente con un dni que ya habia y no debe a�adirlo
	@Test
	void a�adirClienteDNIExistente() {
		Cliente dosCliente = new Cliente("123456D", "Arturo", LocalDate.now());
		unGestor.altaCliente(dosCliente);
		boolean actual = unGestor.getListaClientes().contains(dosCliente);
		assertFalse(actual);
	}
	
	
	
	
}
