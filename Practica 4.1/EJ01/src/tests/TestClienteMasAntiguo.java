package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestClienteMasAntiguo {

	static Cliente unCliente;
	static Cliente dosCliente;
	static Cliente tresCliente;
	static GestorContabilidad unGestor;
	
	@BeforeAll
	public static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		dosCliente = new Cliente("123456F", "Jor", LocalDate.MAX);
		tresCliente = new Cliente("123456F", "Jor", LocalDate.MIN);
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaClientes().clear();
	}
	
	//Busca el cliente mas antiguo entre un cliente y debe retornarlo
	
	@Test
	void buscarClienteMasAntiguoUnCliente() {
		unGestor.getListaClientes().add(unCliente);
		Cliente actual = unGestor.clienteMasAntiguo();
		assertEquals(unCliente, actual);
	}
	
	//Busca el cliente mas antiguo entre tres clientes y debe retornar el mas antiguo
	
	@Test
	void buscarClienteMasAntiguoTresClientes() {
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(dosCliente);
		unGestor.getListaClientes().add(tresCliente);
		Cliente actual = unGestor.clienteMasAntiguo();
		assertEquals(tresCliente, actual);
	}
	
	//Busca el cliente mas antiguo, pero no hay clientes, debe dar null
	
	@Test
	void buscarClienteMasAntiguoSinClientes() {
		Cliente actual = unGestor.clienteMasAntiguo();
		assertNull(actual);
	}

}
