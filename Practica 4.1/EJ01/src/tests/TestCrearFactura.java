package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestCrearFactura {


	static Cliente unCliente;
	static Factura unaFactura;
	static GestorContabilidad unGestor;
	
	@BeforeAll
	public static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, unCliente);
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaFacturas().clear();
		unGestor.getListaClientes().clear();
		unGestor.crearFactura(unaFactura);
	}
	
	//Crea una factura nueva y debe crearla bien
	
	@Test
	void testCrearFacturaInexistente() {
		boolean actual = unGestor.getListaFacturas().contains(unaFactura);
		assertTrue(actual);
	}
	
	//Crea una factura con codigo repetido y no debe crearla
	
	@Test
	void testCrearFacturaExistente() {
		Factura dosFactura = new Factura("1", LocalDate.now(), "Pollo Frito", 7, 9, unCliente);
		unGestor.crearFactura(dosFactura);
		boolean actual = unGestor.getListaFacturas().contains(dosFactura);
		assertFalse(actual);
	}

}
