package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestBuscarFactura {
	
	static Cliente unCliente;
	static Factura unaFactura;
	static GestorContabilidad unGestor;
	
	@BeforeAll
	static void preparar() {
		unCliente = new Cliente("123456F", "Jor", LocalDate.now());
		unaFactura = new Factura("1", LocalDate.now(), "Salchipapas", 5, 6, unCliente);
		unGestor = new GestorContabilidad();
	}
	
	@BeforeEach
	void preparar2() {
		unGestor.getListaFacturas().clear();
		
	}
	
	//Busca una factura que existe y debe encontrarla
	
	@Test
	void buscarFacturaExistente() {
		unGestor.getListaFacturas().add(unaFactura);
		Factura actual = unGestor.buscarFactura("1");
		assertSame(unaFactura, actual);
	}
	
	//Busca una factura que no existe y debe dar null
	
	@Test
	void buscarFacturaInexistente() {
		unGestor.getListaFacturas().add(unaFactura);
		Factura actual = unGestor.buscarFactura("3");
		assertNull(actual);
	}
	
	//Busca una factura sin que haya facturas metidas
	@Test
	void buscarFacturaSinFacturas() {
		Factura actual = unGestor.buscarFactura("1");
		assertNull(actual);
		
	}

}
